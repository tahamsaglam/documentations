package com.mst.demo.material;

import mst.widget.MstListView;
import mst.widget.toolbar.Toolbar;
import mst.widget.toolbar.Toolbar.OnMenuItemClickListener;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RadioButton;
import android.widget.CheckBox;
import android.widget.Switch;
import android.widget.TextView;

import com.mst.demo.R;
import com.mst.demo.util.FragmentUtils;
public class CompoundButtonFragment extends Fragment implements View.OnClickListener{
	private static final String 	TAG = "SliderLayoutFragment";

	private RelativeLayout mEnableLayout;
	private RelativeLayout mRadioLayout;
	private RelativeLayout mRadioLayout1;
	private RelativeLayout mCheckLayout;
	private RelativeLayout mCheckLayout1;
	private RelativeLayout mCheckLayout2;
	private RelativeLayout mSwitchLayout;
	private RelativeLayout mSwitchLayout1;
	private RelativeLayout mSwitchLayout2;

	private Switch mEnable;
	private RadioButton mRadio;
	private RadioButton mRadio1;
	private CheckBox mCheck;
	private CheckBox mCheck1;
	private CheckBox mCheck2;
	private Switch mSwitch;
	private Switch mSwitch1;
	private Switch mSwitch2;
	
    private String[] mItems;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.comboundbutton_layout, container,false);
		mEnableLayout = (RelativeLayout) view.findViewById(R.id.enable_layout);
		mRadioLayout = (RelativeLayout) view.findViewById(R.id.radio_button_layout);
		mRadioLayout1 = (RelativeLayout) view.findViewById(R.id.radio_button_layout1);
		mCheckLayout = (RelativeLayout) view.findViewById(R.id.check_button_layout);
		mCheckLayout1 = (RelativeLayout) view.findViewById(R.id.check_button_layout1);
		mCheckLayout2 = (RelativeLayout) view.findViewById(R.id.check_button_layout2);
		mSwitchLayout = (RelativeLayout) view.findViewById(R.id.switch_button_layout);
		mSwitchLayout1 = (RelativeLayout) view.findViewById(R.id.switch_button_layout1);
		mSwitchLayout2 = (RelativeLayout) view.findViewById(R.id.switch_button_layout2);
		mEnableLayout.setOnClickListener(this);
		mRadioLayout.setOnClickListener(this);
		mRadioLayout1.setOnClickListener(this);
		mCheckLayout.setOnClickListener(this);
		mCheckLayout1.setOnClickListener(this);
		mCheckLayout2.setOnClickListener(this);
		mSwitchLayout.setOnClickListener(this);
		mSwitchLayout1.setOnClickListener(this);
		mSwitchLayout2.setOnClickListener(this);
		mEnable = (Switch) view.findViewById(R.id.enable_button);
		mRadio = (RadioButton) view.findViewById(R.id.radio_button);
		mRadio1 = (RadioButton) view.findViewById(R.id.radio_button1);
		mCheck = (CheckBox) view.findViewById(R.id.check_button);
		mCheck1 = (CheckBox) view.findViewById(R.id.check_button1);
		mCheck2 = (CheckBox) view.findViewById(R.id.check_button2);
		mSwitch = (Switch) view.findViewById(R.id.switch_button);
		mSwitch1 = (Switch) view.findViewById(R.id.switch_button1);
		mSwitch2 = (Switch) view.findViewById(R.id.switch_button2);
		mEnable.setChecked(true);
		return view;
	}

	@Override
	public void onClick(View v){
		int id = v.getId();
		switch(id){
		case R.id.enable_layout:
			mEnable.setChecked(!mEnable.isChecked());
			boolean enable = mEnable.isChecked();
			mRadio.setEnabled(enable);
			mRadio1.setEnabled(enable);
			mCheck.setEnabled(enable);
			mCheck1.setEnabled(enable);
			mCheck2.setEnabled(enable);
			mSwitch.setEnabled(enable);
			mSwitch1.setEnabled(enable);
			mSwitch2.setEnabled(enable);
		break;
		case R.id.radio_button_layout:
			if(mRadio.isEnabled()){
				mRadio.setChecked(!mRadio.isChecked());
			}
		break;
		case R.id.radio_button_layout1:
			if(mRadio1.isEnabled()){
				mRadio1.setChecked(!mRadio1.isChecked());
			}
		break;
		case R.id.check_button_layout:
			if(mCheck.isEnabled()){
				mCheck.setChecked(!mCheck.isChecked());
			}
		break;
		case R.id.check_button_layout1:
			if(mCheck1.isEnabled()){
				mCheck1.setChecked(!mCheck1.isChecked());
			}
		break;
		case R.id.check_button_layout2:
			if(mCheck2.isEnabled()){
				mCheck2.setChecked(!mCheck2.isChecked());
			}
		break;
		case R.id.switch_button_layout:
			if(mSwitch.isEnabled()){
				mSwitch.setChecked(!mSwitch.isChecked());
			}
		break;
		case R.id.switch_button_layout1:
			if(mSwitch.isEnabled()){
				mSwitch1.setChecked(!mSwitch1.isChecked());
			}
		break;
		case R.id.switch_button_layout2:
			if(mSwitch.isEnabled()){
				mSwitch2.setChecked(!mSwitch2.isChecked());
			}
		break;
		}
	}

}
