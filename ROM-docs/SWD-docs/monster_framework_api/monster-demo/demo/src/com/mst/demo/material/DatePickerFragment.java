package com.mst.demo.material;

import java.util.ArrayList;
import java.util.List;

import com.mst.demo.AgendaFragment;
import com.mst.demo.FragmentAdapter;
import com.mst.demo.InfoDetailsFragment;
import com.mst.demo.ShareFragment;

import mst.widget.DatePicker;
import mst.widget.DateTimePicker;
import mst.widget.DatePicker.OnDateChangedListener;
import mst.widget.DrawerLayout;
import mst.widget.NavigationView;
import mst.widget.TimePicker;
import mst.widget.ViewPager;
import mst.widget.TimePicker.OnTimeChangedListener;
import mst.widget.tab.TabLayout;
import mst.widget.toolbar.Toolbar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mst.demo.R;
public class DatePickerFragment extends Fragment {
	private DatePicker mDatePicker;
	private DateTimePicker mDateTimePicker;
	private TextView mResult;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_date_picker, container,false);
		initView(view);
		return view;
	}
	

    private void initView(View view) {
        mDatePicker = (DatePicker)view.findViewById(R.id.date_picker);
        mResult = (TextView) view.findViewById(android.R.id.text1);
        mResult.setText(mDatePicker.getYear()+"."+mDatePicker.getMonth()+"."+mDatePicker.getDayOfMonth());
        mDatePicker.init(2016, 12, 30, new OnDateChangedListener() {
			
			@Override
			public void onDateChanged(DatePicker picker, int year, int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				mResult.setText(year+"."+monthOfYear+"."+dayOfMonth);
			}
		});
		mDateTimePicker = (DateTimePicker)view.findViewById(R.id.date_picker2);
		mDateTimePicker.init(2016, 12, 30, 0, 0, new DateTimePicker.OnDateChangedListener() {

			@Override
			public void onDateChanged(DateTimePicker picker, int year, int monthOfYear, int dayOfMonth, int hour, int minute) {
				// TODO Auto-generated method stub
				mResult.setText(year+"."+monthOfYear+"."+dayOfMonth+"."+hour+"."+minute);
			}
		});
    }
    
    

}
