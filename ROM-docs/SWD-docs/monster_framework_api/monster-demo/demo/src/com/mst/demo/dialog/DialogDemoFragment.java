package com.mst.demo.dialog;

import mst.app.dialog.AlertDialog;
import mst.app.dialog.ProgressDialog;
import mst.app.dialog.DatePickerDialog;
import mst.app.dialog.DateTimeDialog;
import mst.app.dialog.TimePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import mst.widget.MstListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import mst.widget.MstListView;
import mst.widget.DatePicker;
import mst.widget.DateTimePicker;
import mst.widget.TimePicker;

import com.mst.demo.R;

import android.widget.ArrayAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class DialogDemoFragment extends Fragment implements OnItemClickListener {
	
	private static final int DIALOG_ID_WITH_MESSAGE = 0;
	
	private static final int DIALOG_ID_WITH_TITLE = 1;
	
	private static final int DIALGO_ID_WITH_TITLE_ICON = 2;
	
	private static final int DIALOG_ID_WITH_MSG_TITLE = 3;
	
	private static final int DIALOG_ID_WITH_POS_BTN = 4;
	
	private static final int DIALOG_ID_WITH_NEG_BTN = 5;
	
	private static final int DIALOG_ID_WITH_POS_NEG_BTN = 6;
	
	private static final int DIALGO_ID_WITH_HORIZENTAL_PROGRESS = 7;
	
	private static final int DIALOG_ID_WITH_SPINNER_PROGRESS = 8;
	
	private static final int DIALOG_ID_SINGLE_CHOICE = 9;
	
	private static final int DIALOG_ID_MULTIPLE_CHOICE = 10;

	private static final int DIALOG_ID_DATEPICKER = 11;
	private static final int DIALOG_ID_DATETIMEPICKER = 12;

	private static final int DIALOG_ID_TIMEPICKER_IS24HOUR = 13;
	private static final int DIALOG_ID_TIMEPICKER = 14;
	
	private static final int DIALOG_ID_CUSTOM_VIEW = 15;
	private static final int DIALOG_ID_CUSTOM_VIEW1 = 16;
	private static final int DIALOG_ID_DIALOGACTIVITY = 17;

    private MstListView mList;

    private String[] mItems;

    private Context mContext;
    
    
    

    private ProgressDialog mProgressDialog;
    
    private Handler mUpdateProgressHandler = new Handler();
    
    private Runnable mUpdateProgressRunnable = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
				mProgressDialog.setProgress(mProgress);				
		}
	};
    
    private int mProgress = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mList = (MstListView)inflater.inflate(R.layout.dialog_demo_list,container,false);

        mContext = getActivity();

        mItems = mContext.getResources().getStringArray(R.array.dialog_demo_item_array);


        mList.setAdapter(new MyAdapter());
        mList.setOnItemClickListener(this);
        return mList;
    }




	@Override
	public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {
		// TODO Auto-generated method stub
		showDialog(position);
	}

	
	private void showDialog(int dialogId) {
		 Dialog dialog = null;
		switch (dialogId) {
		case DIALOG_ID_WITH_MESSAGE:
			dialog.show();
			dialog = new AlertDialog.Builder(getActivity()).
					setMessage("是否删除此联系人是否删除此联系人是否删除此联系人是否删除此联系人").
					setNegativeButton("取消", null).setPositiveButton("确定", null).create();
			
			break;
		case DIALOG_ID_WITH_TITLE :
			dialog = new AlertDialog.Builder(getActivity()).
					setTitle("标题标题").
					setMessage("是否删除此联系人是否删除此联系人是否删除此联系人是否删除此联系人").
					setNegativeButton("取消", null).setPositiveButton("确定", null).create();
			break;
		case DIALGO_ID_WITH_TITLE_ICON:
			dialog = new AlertDialog.Builder(getActivity()).
					setTitle("标题标题").
					setIcon(android.R.drawable.ic_dialog_alert).
					setMessage("是否删除此联系人是否删除此联系人是否删除此联系人是否删除此联系人").
					setNegativeButton("取消", null).setPositiveButton("确定", null).create();
			break;
		case DIALOG_ID_WITH_MSG_TITLE:
			dialog = new AlertDialog.Builder(getActivity()).
					setTitle("标题标题").
					setMessage("是否删除此联系人是否删除此联系人是否删除此联系人是否删除此联系人").create();
			break;
		case DIALOG_ID_WITH_POS_BTN:
			dialog = new AlertDialog.Builder(getActivity()).setMessage("Msg")
			.setTitle("Title").setPositiveButton("OK", null).create();
			break;
		case DIALOG_ID_WITH_NEG_BTN:
			dialog = new AlertDialog.Builder(getActivity()).setMessage("Msg")
			.setTitle("标题").setNegativeButton("Cancle", null).create();
			break;
		case DIALOG_ID_WITH_POS_NEG_BTN:
			dialog = new AlertDialog.Builder(getActivity()).setMessage("是否删除此联系人")
			.setTitle("标题").setNegativeButton("取消", null).setPositiveButton("确定", null).create();
			break;
		case DIALGO_ID_WITH_HORIZENTAL_PROGRESS:
			mProgressDialog  = new ProgressDialog(getActivity());
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setTitle("标题");
			mProgressDialog.setMessage("内容内容内容");
			mProgressDialog.setMax(100);
			DialogInterface.OnClickListener linstener = new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Toast.makeText(getActivity(),""+which,Toast.LENGTH_SHORT).show();
					mProgressDialog.dismiss();
					mProgress = 100;
				}
			};
			mProgressDialog.setButton(DialogInterface.BUTTON_POSITIVE,"确定",linstener);
			mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,"取消",linstener);
			mProgress = 0;
			mProgressDialog.setProgress(0);
			mProgressDialog.show();
			new Thread() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					while (mProgress < 100) {
						try{
						Thread.sleep(500);
						mProgress ++ ;
						mUpdateProgressHandler.post(mUpdateProgressRunnable);
						}catch(Exception e){
							
						}
						
					}
				}
			}.start();;
			
			break;
		case DIALOG_ID_WITH_SPINNER_PROGRESS:
			mProgressDialog  = new ProgressDialog(getActivity());
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.setMessage("加载中...");
			mProgressDialog.show();
			break;
		case DIALOG_ID_SINGLE_CHOICE:
			AlertDialog dialogSingleChoice = new AlertDialog.Builder(getActivity())
			.setSingleChoiceItems(new String[]{"item1","item2","item3"}, 0, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int witch) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity(), "Selected Item "+witch, Toast.LENGTH_SHORT).show();
					//dialog.dismiss();
				}
			}).setTitle("标题标题").setNegativeButton("取消", null).setPositiveButton("确定", null).create();
			dialogSingleChoice.show();
			break;
		case DIALOG_ID_MULTIPLE_CHOICE:
			final AlertDialog dialogMultiple = new AlertDialog.Builder(getActivity())
			.setMultiChoiceItems(new String[]{"item1","item2","item3"}, new boolean[]{true,true,false}, 
					new DialogInterface.OnMultiChoiceClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int witch, boolean selected) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity(), "Item "+witch +" is Selected:"+selected, Toast.LENGTH_SHORT).show();
				}
			}).setTitle("标题标题").setNegativeButton("取消", null).setPositiveButton("确定", null).create();
			dialogMultiple.show();
			break;
		case DIALOG_ID_DATEPICKER:
			DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {
		                @Override
		                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
					Toast.makeText(getActivity(), year+"-"+monthOfYear+"-"+dayOfMonth, Toast.LENGTH_SHORT).show();
		                }
		            });
			datePickerDialog.show();
			break;
		case DIALOG_ID_DATETIMEPICKER:
			DateTimeDialog dateTimeDialog = new DateTimeDialog(getActivity(),new DateTimeDialog.OnDateTimeSetListener() {
				@Override
				public void onDateTimeSet(DateTimePicker view, int year, int monthOfYear, int dayOfMonth, int hour, int minute) {
					Toast.makeText(getActivity(), year+"-"+monthOfYear+"-"+dayOfMonth+"-"+hour+"-"+minute, Toast.LENGTH_SHORT).show();
				}
			});
			dateTimeDialog.show();
			break;
		case DIALOG_ID_TIMEPICKER_IS24HOUR:
			TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),new TimePickerDialog.OnTimeSetListener() {
		                @Override
		                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					Toast.makeText(getActivity(), hourOfDay+":"+minute, Toast.LENGTH_SHORT).show();
		                }
		            },true);
			timePickerDialog.show();
			break;
		case DIALOG_ID_TIMEPICKER:
			TimePickerDialog timePickerDialog1 = new TimePickerDialog(getActivity(),new TimePickerDialog.OnTimeSetListener() {
		                @Override
		                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					Toast.makeText(getActivity(), hourOfDay+":"+minute, Toast.LENGTH_SHORT).show();
		                }
		            },false);
			timePickerDialog1.show();
			break;
		case DIALOG_ID_CUSTOM_VIEW:
			EditText editText = new EditText(getActivity());
			editText.setBackgroundResource(com.mst.R.drawable.edit_text_material);
			AlertDialog customDialog = new AlertDialog.Builder(getActivity()).setTitle("设备名称")
			.setView(editText).create();
			customDialog.show();
			
			break;
		case DIALOG_ID_CUSTOM_VIEW1:

			AlertDialog customDialog1 = new AlertDialog.Builder(getActivity()).setTitle("设备名称")
			.setView(R.layout.dialog_custom_layout)
					.setPositiveButton("确定",null)
					.setNegativeButton("取消",null).create();
			customDialog1.show();

			break;
		case DIALOG_ID_DIALOGACTIVITY:
			Intent intent = new Intent(getActivity(),MyDialogActivity.class);
			getActivity().startActivity(intent);
			break;

		}
		
		if(dialog != null){
			dialog.show();
		}
	}
	

	class MyAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mItems.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mItems[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView title = (TextView) LayoutInflater.from(mContext).inflate(com.mst.R.layout.list_item_1_line, null);
			title.setText(mItems[position]);
			return title;
		}
		
	}





}
