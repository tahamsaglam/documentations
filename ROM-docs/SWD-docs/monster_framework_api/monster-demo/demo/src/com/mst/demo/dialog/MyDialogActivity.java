package com.mst.demo.dialog;

import android.os.Bundle;

import mst.app.AlertActivity;

/**
 * Created by caizhongting on 16-10-25.
 */
public class MyDialogActivity extends AlertActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAlertParams.mTitle="标题";
        mAlertParams.mMessage="测试测试测试";
        mAlertParams.mPositiveButtonText="确定";
        mAlertParams.mNegativeButtonText="cancel";
        setupAlert();
    }
}
