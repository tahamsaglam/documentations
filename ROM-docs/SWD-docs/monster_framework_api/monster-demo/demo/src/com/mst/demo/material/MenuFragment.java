package com.mst.demo.material;

import mst.view.menu.BottomWidePopupMenu;
import mst.view.menu.PopupMenu;
import mst.view.menu.PopupMenu.OnMenuItemClickListener;
import mst.widget.DatePicker;
import mst.widget.DatePicker.OnDateChangedListener;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mst.demo.R;
public class MenuFragment extends Fragment implements OnClickListener{
	private Button mShowPopupMenuBtn;
	private Button mShowButtonListMenuBtn;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_menu, container,false);
		initView(view);
		return view;
	}
	

    private void initView(View view) {
    	mShowPopupMenuBtn = (Button)view.findViewById(android.R.id.button1);
    	mShowButtonListMenuBtn = (Button)view.findViewById(android.R.id.button2);
    	mShowPopupMenuBtn.setOnClickListener(this);
    	mShowButtonListMenuBtn.setOnClickListener(this);
    }


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v == mShowPopupMenuBtn){
			//初始化PopupMenu
			PopupMenu popupMenu = new PopupMenu(getActivity(), mShowPopupMenuBtn,Gravity.TOP);
			//设置popupmenu 中menu的点击事件
			popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity(), "item "+item.getItemId()+"  clicked", Toast.LENGTH_SHORT).show();
					return false;
				}
			});
			//导入menu布局
			popupMenu.inflate(R.menu.main);
			//显示popup menu
			popupMenu.show();
		}else if(v == mShowButtonListMenuBtn){
			//初始化底部菜单
			BottomWidePopupMenu menu = new BottomWidePopupMenu(getContext());
			//加载menu文件
			menu.inflateMenu(R.menu.bottom_menu);
			//设置menu item点击事件
			menu.setOnMenuItemClickedListener(new BottomWidePopupMenu.OnMenuItemClickListener() {
				
				@Override
				public boolean onItemClicked(MenuItem item) {
					// TODO Auto-generated method stub
					Toast.makeText(getActivity(), "Item:"+item.getTitle(), Toast.LENGTH_SHORT).show();
					return false;
				}

			});
			//显示menu窗口
			menu.show();
		}
	}
    
    

}
