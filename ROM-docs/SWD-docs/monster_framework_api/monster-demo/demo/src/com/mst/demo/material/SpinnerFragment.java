package com.mst.demo.material;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.MstSpinner;
import android.widget.TextView;

import com.mst.demo.R;
public class SpinnerFragment extends Fragment {
	private MstSpinner mCustomSpinner;
	private String[] mSpinnerItems;
	private CustomSpinnerAdapter mAdapter;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.md_spinner, container, false);
		initView(view);
		return view;
	}

	private void initView(View view) {
		mCustomSpinner = (MstSpinner)view.findViewById(R.id.custom_spinner);
		mSpinnerItems = getResources().getStringArray(R.array.items_entry);
		mAdapter = new CustomSpinnerAdapter();
		mCustomSpinner.setAdapter(mAdapter);
	}
	
	
	class CustomSpinnerAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mSpinnerItems.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mSpinnerItems[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		
		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			convertView  = LayoutInflater.from(getActivity()).inflate(android.R.layout.simple_list_item_2, null);
			TextView itemText = (TextView)convertView.findViewById(android.R.id.text1);
			itemText.setText(mSpinnerItems[position]);
			itemText.setTextColor(Color.RED);
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView itemText = (TextView) LayoutInflater.from(getActivity()).inflate(android.R.layout.simple_list_item_1, null);
			itemText.setText(mSpinnerItems[position]);
			return itemText;
		}
		
	}
	
	
}
